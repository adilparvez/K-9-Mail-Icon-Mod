package uk.co.adilparvez.k_9mailiconmod;


import android.content.res.XModuleResources;

import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;

public class K9MailMod {

    private static String MODULE_PATH;

    public static void initZygote(IXposedHookZygoteInit.StartupParam startupParam) throws Throwable {
        MODULE_PATH = startupParam.modulePath;
    }

    public static void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam initPackageResourcesParam) throws Throwable {
        if (!initPackageResourcesParam.packageName.equals("com.fsck.k9")) {
            return;
        }

        XModuleResources moduleResources = XModuleResources.createInstance(MODULE_PATH, initPackageResourcesParam.res);
        initPackageResourcesParam.res.setReplacement("com.fsck.k9:drawable/icon", moduleResources.fwd(R.drawable.ic_launcher));
    }

}
